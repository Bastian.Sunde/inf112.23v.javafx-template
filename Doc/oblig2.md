Team Møte 24.2:
Hva vi har gjort:
Vi har ikke jobbet med noe serlig siden innlevering forrige uke.

Til neste uke så plannlegger vi:
Elias: se på AIs og enemies og sånn
Snorre: jobbe med vektorisering
Kasper: Skal utvide spillebrettet of fikse på miljøet
Bastian: skal se på lyd og tester.

Team Møte 3/3:
Hva vi har gjort:
Vi har fikset generel abstraktjon, som gravity og gameactors osv.

Til neste uke så plannlegger vi:
Elias: studere hvordan enemies fungerer i spill utvikling og legge til sprite og animasjon.
Snorre: legge til karakter states og fikse opp i gameactor normalilzation. se på HUD.
Kasper: lagt til sprits og designe konsept for spillbrett.
Bastian: Se på integrasjon av lyd i spillet.

Team Møte 10/3:
Hva vi har gjort:
Vi har lagt til sprites og animasjoner for enemy og karakter. vi har lagt til sprites for planneter og fikset på gravity. vi har lagt til en simpel HUD.

Til neste uke så plannlegger vi:
Elias: ha enemy bevege seg rundt med logik.
Snorre: legge til attack states og hitboxes.
Kasper: Lagge ferdig tutorial planet. outlaye map.
Bastian: finne ut lyder og se på integrasjon.




Prosjektrapport:
* Hvordan fungerer rollene i teamet? Trenger dere å oppdatere hvem som er teamlead eller kundekontakt?

Rollene har fungerert bra. Vi tar ansvar for de rollene vi har blitt gitt.

* Trenger dere andre roller? Skriv ned noen linjer om hva de ulike rollene faktisk innebærer for dere.

Rollene våre har vært nokk til å beskrive være nærtidlige problemer og ansvar, men vi kan se at vi må kanskje ta på nye anvsvar i framtidige team møter når våre gamle roller ikke er nødvendige lenger.


* Er det noen erfaringer enten team-messig eller mtp prosjektmetodikk som er verdt å nevne? Synes teamet at de valgene dere har tatt er gode? Hvis ikke, hva kan dere gjøre annerledes for å forbedre måten teamet fungerer på?

Vi føler oss konfortabel med valgene vi har tatt i forhold til utviklingen av prosjektet.

* Hvordan er gruppedynamikken? Er det uenigheter som bør løses?

Gruppedynamikken er hyggelig og avslappet, alle kommer overens fint.

* Hvordan fungerer kommunikasjonen for dere?

I gruppetimene er det god dialog og diskusjon. Over discord kan det være litt vanskelig siden folk er sjelden online på samme tid, men vi føler oss forsatt veldig konfortabel med vår fremgang.

* Gjør et kort retrospektiv hvor dere vurderer hva dere har klart til nå, og hva som kan forbedres. Dette skal handle om prosjektstruktur, ikke kode. Dere kan selvsagt diskutere kode, men dette handler ikke om feilretting, men om hvordan man jobber og kommuniserer.

Vi føler at vi har kommet oss godt i gang og har lagd grunnlaget for et veldig kult spill. Vi kunne ha hatt vært litt mere objekt-orientert og modulær i programmeringen vår tidligere i koden, det ville ha kanskje spart oss litt tid i noen steder av prosjektet. Vi hadde litt forskjellige syn av hvordan spille slutt-resultatet skulle se ut tidlig i prosjektet, men vi er nå alle "på samme side".

* Under vurdering vil det vektlegges at alle bidrar til kodebasen. Hvis det er stor forskjell i hvem som committer, må dere legge ved en kort forklaring for hvorfor det er sånn. Husk å committe alt. (Også designfiler)

Den største committeren er Sondre, han har mest erfaring og tid i spill-design. Dette har latt ham gi oss et godt grunnlag for å kode ut fra.

* Bli enige om maks tre forbedringspunkter fra retrospektivet, som skal følges opp under neste sprint.

Tre forbedringspunkter kan være:
 - Kommunikasjon utenfor gruppetimene.
 - Større fremgang i kode mellom gruppetimer.
 - Modulærlitet i koden og kodestil.

## Produkt og kode

* Dette har vi fikset siden sist:
    Vi har ikke fikset noe siden sist ettersom vi ikke hadde noen bugs eller problemer fra forrige innlevering

* Vi har ingen kjente feil ved cross-platform bruk, vi har testet spillet på både windows og Mac og dette fungerer som det skal.
* Vi har ikke fått testet på Linux, men etter overgang over prosjektet har vi erklært at det vil kjøre fint der og.

* Vi har ikke tatt i bruk statiske analyseverktøy og har dermed ingenting å formidle angående hva vi fant / om det var nyttig.